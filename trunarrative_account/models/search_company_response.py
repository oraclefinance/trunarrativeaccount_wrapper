# coding: utf-8

"""
    Companies

    Public REST API for account module  ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_account.models.search_company import SearchCompany  # noqa: F401,E501


class SearchCompanyResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'page_number': 'int',
        'kind': 'str',
        'total_results': 'int',
        'items': 'list[SearchCompany]',
        'items_per_page': 'int',
        'start_index': 'int'
    }

    attribute_map = {
        'page_number': 'page_number',
        'kind': 'kind',
        'total_results': 'total_results',
        'items': 'items',
        'items_per_page': 'items_per_page',
        'start_index': 'start_index'
    }

    def __init__(self, page_number=None, kind='', total_results=None, items=None, items_per_page=None, start_index=None):  # noqa: E501
        """SearchCompanyResponse - a model defined in Swagger"""  # noqa: E501

        self._page_number = None
        self._kind = None
        self._total_results = None
        self._items = None
        self._items_per_page = None
        self._start_index = None
        self.discriminator = None

        if page_number is not None:
            self.page_number = page_number
        if kind is not None:
            self.kind = kind
        if total_results is not None:
            self.total_results = total_results
        if items is not None:
            self.items = items
        if items_per_page is not None:
            self.items_per_page = items_per_page
        if start_index is not None:
            self.start_index = start_index

    @property
    def page_number(self):
        """Gets the page_number of this SearchCompanyResponse.  # noqa: E501


        :return: The page_number of this SearchCompanyResponse.  # noqa: E501
        :rtype: int
        """
        return self._page_number

    @page_number.setter
    def page_number(self, page_number):
        """Sets the page_number of this SearchCompanyResponse.


        :param page_number: The page_number of this SearchCompanyResponse.  # noqa: E501
        :type: int
        """

        self._page_number = page_number

    @property
    def kind(self):
        """Gets the kind of this SearchCompanyResponse.  # noqa: E501


        :return: The kind of this SearchCompanyResponse.  # noqa: E501
        :rtype: str
        """
        return self._kind

    @kind.setter
    def kind(self, kind):
        """Sets the kind of this SearchCompanyResponse.


        :param kind: The kind of this SearchCompanyResponse.  # noqa: E501
        :type: str
        """

        self._kind = kind

    @property
    def total_results(self):
        """Gets the total_results of this SearchCompanyResponse.  # noqa: E501


        :return: The total_results of this SearchCompanyResponse.  # noqa: E501
        :rtype: int
        """
        return self._total_results

    @total_results.setter
    def total_results(self, total_results):
        """Sets the total_results of this SearchCompanyResponse.


        :param total_results: The total_results of this SearchCompanyResponse.  # noqa: E501
        :type: int
        """

        self._total_results = total_results

    @property
    def items(self):
        """Gets the items of this SearchCompanyResponse.  # noqa: E501


        :return: The items of this SearchCompanyResponse.  # noqa: E501
        :rtype: list[SearchCompany]
        """
        return self._items

    @items.setter
    def items(self, items):
        """Sets the items of this SearchCompanyResponse.


        :param items: The items of this SearchCompanyResponse.  # noqa: E501
        :type: list[SearchCompany]
        """

        self._items = items

    @property
    def items_per_page(self):
        """Gets the items_per_page of this SearchCompanyResponse.  # noqa: E501


        :return: The items_per_page of this SearchCompanyResponse.  # noqa: E501
        :rtype: int
        """
        return self._items_per_page

    @items_per_page.setter
    def items_per_page(self, items_per_page):
        """Sets the items_per_page of this SearchCompanyResponse.


        :param items_per_page: The items_per_page of this SearchCompanyResponse.  # noqa: E501
        :type: int
        """

        self._items_per_page = items_per_page

    @property
    def start_index(self):
        """Gets the start_index of this SearchCompanyResponse.  # noqa: E501


        :return: The start_index of this SearchCompanyResponse.  # noqa: E501
        :rtype: int
        """
        return self._start_index

    @start_index.setter
    def start_index(self, start_index):
        """Sets the start_index of this SearchCompanyResponse.


        :param start_index: The start_index of this SearchCompanyResponse.  # noqa: E501
        :type: int
        """

        self._start_index = start_index

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, SearchCompanyResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
