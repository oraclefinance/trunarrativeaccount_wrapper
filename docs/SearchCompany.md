# SearchCompany

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_status** | **str** |  | [optional] [default to '']
**address_snippet** | **str** |  | [optional] [default to '']
**date_of_creation** | **date** |  | [optional] 
**matches** | [**Match**](Match.md) |  | [optional] 
**snippet** | **str** |  | [optional] [default to '']
**description** | **str** |  | [optional] [default to '']
**links** | [**Link**](Link.md) |  | [optional] 
**company_number** | **str** |  | [optional] [default to '']
**title** | **str** |  | [optional] [default to '']
**company_type** | **str** |  | [optional] [default to '']
**address** | [**Address**](Address.md) |  | [optional] 
**kind** | **str** |  | [optional] [default to '']
**description_identifier** | **list[str]** |  | [optional] 
**date_of_cessation** | **date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


