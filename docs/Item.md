# Item

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**Address**](Address.md) |  | [optional] 
**name** | **str** |  | [optional] [default to '']
**appointed_on** | **date** |  | [optional] 
**officer_role** | **str** |  | [optional] [default to '']
**links** | [**OfficerLink**](OfficerLink.md) |  | [optional] 
**date_of_birth** | [**DateOfBirth**](DateOfBirth.md) |  | [optional] 
**occupation** | **str** |  | [optional] [default to '']
**country_of_residence** | **str** |  | [optional] [default to '']
**nationality** | **str** |  | [optional] [default to '']
**resigned_on** | **date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


