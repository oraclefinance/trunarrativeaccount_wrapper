# Officerlist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_index** | **int** |  | [optional] 
**etag** | **str** |  | [optional] [default to '']
**inactive_count** | **int** |  | [optional] 
**links** | [**Link**](Link.md) |  | [optional] 
**kind** | **str** |  | [optional] [default to '']
**items_per_page** | **int** |  | [optional] 
**items** | [**list[Item]**](Item.md) |  | [optional] 
**active_count** | **int** |  | [optional] 
**total_results** | **int** |  | [optional] 
**resigned_count** | **int** |  | [optional] 
**errors** | [**Errors**](Errors.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


