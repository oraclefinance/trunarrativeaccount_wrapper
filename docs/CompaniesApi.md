# trunarrative_account.CompaniesApi

All URIs are relative to *https://localhost/TruAccountAPI/rest/Companies*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_officers**](CompaniesApi.md#get_officers) | **GET** /v1/Officers | 
[**search_company**](CompaniesApi.md#search_company) | **GET** /v1/Search | 


# **get_officers**
> Officerlist get_officers(company_number)





### Example
```python
from __future__ import print_function
import time
import trunarrative_account
from trunarrative_account.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_account.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_account.CompaniesApi(trunarrative_account.ApiClient(configuration))
company_number = '' # str |  (default to )

try:
    api_response = api_instance.get_officers(company_number)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CompaniesApi->get_officers: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **company_number** | **str**|  | [default to ]

### Return type

[**Officerlist**](Officerlist.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_company**
> SearchCompanyResponse search_company(query, items_per_page=items_per_page, start_index=start_index)





### Example
```python
from __future__ import print_function
import time
import trunarrative_account
from trunarrative_account.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_account.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_account.CompaniesApi(trunarrative_account.ApiClient(configuration))
query = '' # str |  (default to )
items_per_page = 0 # int |  (optional)
start_index = 0 # int |  (optional)

try:
    api_response = api_instance.search_company(query, items_per_page=items_per_page, start_index=start_index)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CompaniesApi->search_company: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **str**|  | [default to ]
 **items_per_page** | **int**|  | [optional] 
 **start_index** | **int**|  | [optional] 

### Return type

[**SearchCompanyResponse**](SearchCompanyResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

