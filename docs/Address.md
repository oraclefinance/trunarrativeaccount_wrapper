# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**premises** | **str** |  | [optional] [default to '']
**postal_code** | **str** |  | [optional] [default to '']
**country** | **str** |  | [optional] [default to '']
**locality** | **str** |  | [optional] [default to '']
**region** | **str** |  | [optional] [default to '']
**address_line_1** | **str** |  | [optional] [default to '']
**address_line_2** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


