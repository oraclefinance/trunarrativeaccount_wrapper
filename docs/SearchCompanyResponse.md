# SearchCompanyResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page_number** | **int** |  | [optional] 
**kind** | **str** |  | [optional] [default to '']
**total_results** | **int** |  | [optional] 
**items** | [**list[SearchCompany]**](SearchCompany.md) |  | [optional] 
**items_per_page** | **int** |  | [optional] 
**start_index** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


