# String

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | **str** |  | [optional] [default to '']
**error_values** | [**list[ErrorValueItem]**](ErrorValueItem.md) |  | [optional] 
**location** | **str** |  | [optional] [default to '']
**location_type** | **str** |  | [optional] [default to '']
**type** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


